#include "qgraphicsproxywidgetdemo.hpp"
#include <QApplication>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  QGraphicsProxyWidgetDemo w;
  w.show();

  return a.exec();
}
