#-------------------------------------------------
#
# Project created by QtCreator 2016-02-29T22:38:34
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QGraphicsProxyWidgetDemo
TEMPLATE = app


SOURCES += main.cpp\
        qgraphicsproxywidgetdemo.cpp \
    scenewidgetitem.cpp

HEADERS  += qgraphicsproxywidgetdemo.hpp \
    scenewidgetitem.hpp

FORMS    += qgraphicsproxywidgetdemo.ui
