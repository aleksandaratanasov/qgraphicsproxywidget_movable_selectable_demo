#ifndef QGRAPHICSPROXYWIDGETDEMO_HPP
#define QGRAPHICSPROXYWIDGETDEMO_HPP

#include <QWidget>
#include <QPoint>

namespace Ui {
  class QGraphicsProxyWidgetDemo;
}

class QGraphicsProxyWidgetDemo : public QWidget
{
    Q_OBJECT

  public:
    explicit QGraphicsProxyWidgetDemo(QWidget *parent = 0);
    ~QGraphicsProxyWidgetDemo();

  private:
    Ui::QGraphicsProxyWidgetDemo *ui;
    void addWidgetToScene(QPoint initPos);
};

#endif // QGRAPHICSPROXYWIDGETDEMO_HPP
